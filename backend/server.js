const express = require('express');
const cors = require('cors');

const app = express();
const db = require('./messagesDb');
const messages = require('./app/messages');
const port = 8000;

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

db.init().then(() => {
    app.use('/messages', messages(db));
    app.listen(port);
})