const fs = require('fs');
const nanoid = require('nanoid');

let posts = null;

module.exports = {
    init: () => {
        return new Promise((resolve, reject) => {
            fs.readFile('./db.json', (err, result) => {
                if(err) {
                    reject(err);
                } else {
                    posts = JSON.parse(result);
                    resolve(posts);
                }
            })
        });
    },
    getPosts: () => posts,
    addPost: (post) => {
        console.log(post, 'in messagesDb');
        post.id = nanoid();
        posts.push(post);

        let contents = JSON.stringify(posts, null, 2);

        return new Promise((resolve, reject) => {
            fs.writeFile('./db.json', contents, err => {
                if (err) {
                    reject(err);
                } else {
                    resolve(post);
                }
            });
        });
    }
}