const express = require('express');
const router = express.Router();

const multer = require('multer');
const config = require('../config');
const nanoid = require('nanoid');
const path = require('path');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

const createRouter = db => {
    router.get('/', (req, res) => {
        res.send(db.getPosts());
    });

    router.post('/', upload.single('image'), (req, res) => {
        const post = req.body;
        console.log(post)
        if(req.file) {
            post.image = req.file.filename;
        }
        db.addPost(post).then(result => res.send(result));
    });

    return router;
};

module.exports = createRouter;

