import axios from 'axios';
import {LOAD_SUCCESS} from "./actionTypes";


export const getSuccess = posts => ({type: LOAD_SUCCESS, posts});
export const getPosts = () => {
    return (dispatch) => {
        axios.get('/messages').then(response => {
            dispatch(getSuccess(response.data));
        })
    }
};


export const addSuccess = post => ({type: LOAD_SUCCESS, post});
export const addPost = (post) => {
    return (dispatch) => {
        axios.post('/messages', post).then(response => {
            console.log(response.data, 'in addPost response.data')
            dispatch(addSuccess(response.data));
        })
    }
};