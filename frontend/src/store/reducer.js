

import {ADD_SUCCESS, LOAD_SUCCESS} from "./actionTypes";

const initialState = {
    posts: []
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case LOAD_SUCCESS:
            return {...state, posts: action.posts};
        case ADD_SUCCESS:
            const posts = [...state.posts];
            posts.push(action.post);
            console.log(posts, 'posts in reducer');
            return {...state, posts};
        default:
            return state;
    }
};

export default reducer;