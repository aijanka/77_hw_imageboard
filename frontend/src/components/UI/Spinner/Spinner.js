import React from 'react';
import './Spinner.css';

const Spinner = () => (
    <div className="lds-heart"><div/></div>
);

export default Spinner;