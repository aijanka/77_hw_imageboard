import React from 'react';
import {Image} from 'react-bootstrap';
import imageNotAvailable from '../../assets/images/image_not_available.png';
import {apiURL} from "../../constants";

const styles = {
    width: '100px',
    height: '100px',
    margin: '10px',

};

const ProductThumbnail = props => {
    let image = imageNotAvailable;

    if(props.image) {
        image = apiURL + '/uploads/' + props.image;
    }

    return <Image src={image} style={styles} thumbnail/>
};

export default ProductThumbnail;