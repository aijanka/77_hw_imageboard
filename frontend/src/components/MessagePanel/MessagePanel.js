import React from 'react';
import Panel from "react-bootstrap/es/Panel";
import ProductThumbnail from "../ProductThumbnail/ProductThumbnail";

const MessagePanel = props => {
    return (
        <Panel  bsStyle="success">
            <Panel.Heading>
                <Panel.Title componentClass="h3">
                    {props.author}
                </Panel.Title>
            </Panel.Heading>
            <Panel.Body>{props.message}</Panel.Body>
            <ProductThumbnail image={props.image} />
        </Panel>
    )
};

export default MessagePanel;