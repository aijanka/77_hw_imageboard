import React, {Component} from 'react';
import './App.css';
import Posts from "./containers/Posts/Posts";
import SendMessageForm from "./containers/SendMessageForm/SendMessageForm";
import {addPost} from "./store/actions";
import {connect} from "react-redux";
import {Grid} from "react-bootstrap";

class App extends Component {

    state = {
        message: '',
        author: '',
        img: ''
    };

    saveCurrentMessage = (event) => {
        console.log(event);
        this.setState({message: event.target.value});
    };

    saveCurrentAuthor = (event) => {
        this.setState({author: event.target.value});
    };

    saveCurrentImage = event => {
        this.setState({[event.target.name]: event.target.files[0]});

    };

    addPost = event => {
        event.preventDefault();
        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });
        this.props.addPost(formData);
        this.setState({message: '', author: '', img: ''})
        // this.forceUpdate(); //get rid of it
    };

    render() {
        return (
            <Grid>
                <Posts/>
                <SendMessageForm
                    currentMessage={(event) => this.saveCurrentMessage(event)}
                    currentAuthor={(event) => this.saveCurrentAuthor(event)}
                    currentImage={event => this.saveCurrentImage(event)}
                    img={this.state.img}
                    author={this.state.author}
                    message={this.state.message}
                    addPost={this.addPost}
                />
            </Grid>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    addPost: post => dispatch(addPost(post))
});


export default connect(null, mapDispatchToProps)(App);
