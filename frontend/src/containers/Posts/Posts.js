import React, {Component} from 'react';
import {connect} from "react-redux";
import MessagePanel from "../../components/MessagePanel/MessagePanel";
import {getPosts} from "../../store/actions";
import './Posts.css';
import Spinner from "../../components/UI/Spinner/Spinner";

class Posts extends Component {

    scrollToBottom = () => {
        this.messagesEnd.scrollIntoView({ behavior: "smooth" });
    };

    componentDidMount() {
        this.props.getPosts();
        this.scrollToBottom();
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }

    shouldComponentUpdate (nextProps, nextState) {
        return nextProps.posts.length !== this.props.posts.length;
    }

    render() {
        return (
                <div className="messagesWrapper">
                    {!this.props.posts ? <Spinner/> : this.props.posts.map((post, index) => (
                            <MessagePanel
                                key={(Date.now() + index).toString()}
                                message={post.message}
                                author={post.author}
                                image={post.image}
                            />
                        )
                    )}
                    <div style={{ float:"left", clear: "both" }} ref={(el) => { this.messagesEnd = el; }}/>
                </div>
        );
    }
}

const mapStateToProps = state => ({
    posts: state.posts
});

const mapDispatchToProps = dispatch => ({
    getPosts: () => dispatch(getPosts())
});

export default connect(mapStateToProps, mapDispatchToProps)(Posts);